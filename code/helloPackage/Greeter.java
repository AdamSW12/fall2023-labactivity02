package helloPackage;
import java.util.Scanner;
import java.util.Random;
import secondPackage.*;

public class Greeter{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Utilities util = new Utilities();

        System.out.println("enter an integer value");
        int num = scan.nextInt();

        num = util.doubleMe(num);

        System.out.println(num);

        scan.close();
    }

}